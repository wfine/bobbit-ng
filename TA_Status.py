# I don't know if this counts given its similarity to taunts(it's basically a copy with new messages and a slightly different call)
# I also am not confident it works
# But I figured this might be useful regardless




import re

# Metadata

NAME = 'status'
ENABLE = true
TYPE = 'command'
PATTERN = '!status (?P<status>[0-9] +$)')

Usage = '''Usage: !status <number (1-6)>
Display the status of the TA given the bobbit command.
Example:
  > !status 1
  OH
'''

STATUSES = {
  
   '1'  : 'OH',
   '2'  : 'Available for slack questions',
   '3'  : 'Unavailable for questions',
   '4'  : 'Willing to hold extra OH',
   '5'  : 'Will be in lecture',
   '6'  : 'I am not a TA',
} 

#Command
def command(bot, nick, message, channel, status):
    status = STATUSES.get(status, None)
    if status and not channel in bot.suppress_statuses:
      bot.send_message(status, None if channel else nick, channel)

#Register

def register(bot):
  bot.supress_statuses = set()
  return(
    (PATTERN, status),
  )

